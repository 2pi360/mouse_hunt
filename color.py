'''
Operations with color
'''


# (list of aliases): Color
COLOR_LIBRARY = {
    ('transparent', 'a', 'A'):  (  0,   0,   0,   0),

    ('white',     0, 'w', 'W'): (255, 255, 255),
    ('black', 1, 10, 'k', 'K'): (  0,   0,   0),

    ('red',       2, 'r'): (180,   0,   0),
    ('green',     4, 'g'): ( 30, 200,  30),
    ('blue',      3, 'b'): ( 10,  10, 170),
    ('yellow',    5, 'y'): (200, 170,  20),
    ('magenta',   6, 'm'): (160,   0, 170),
    ('cyan',      7, 'c'): (  0, 160, 170),
    ('orange',    8, 'o'): (242, 130,  55),
    ('purple',    9, 'p'): (100,   0, 200),

    ('pure red',     'R'): (255,   0,   0),
    ('pure green',   'G'): (  0, 255,   0),
    ('pure blue',    'B'): (  0,   0, 255),
    ('pure yellow',  'Y'): (255, 255,   0),
    ('pure magenta', 'M'): (255,   0, 255),
    ('pure cyan',    'C'): (  0, 255, 255),
    ('pure orange',  'O'): (250, 100,   0),
    ('pure purple',  'P'): (128,   0, 255),

    ('caption'):            (  0,   0,   0),
    ('label', 'l', 'L'):    ( 50,  50,  50),
    ('axis name'):          ( 50,  50,  50),
    ('axis', 'X', 'x'):     (150, 150, 150),
    ('ticks'):              (150, 150, 150),
    ('grid', '#'):          (205, 205, 205),

    ('background', 'bg'):   (255, 255, 255),

    'debug plot area':      (255, 230, 220),
    'debug plot margins':   (210, 255, 220),
    'debug panel margins':  (200, 150, 255),
    'debug figure margins': (190, 140, 100)
    }

# open up aliases
COLOR_LIBRARY = {name: color for aliases, color in COLOR_LIBRARY.items() for name in (aliases if isinstance(aliases, tuple) else [aliases]) }


def _to_0_255(x):
    return 0 if x < 0 else 255 if x > 255 else int(x)


def merge(color0, color1, fraction, alpha=None):
    r0, g0, b0, a0 = Color(color0)
    r1, g1, b1, a1 = Color(color1)
    return Color.from_RGB(
        r0*fraction + r1*(1-fraction),
        g0*fraction + g1*(1-fraction),
        b0*fraction + b1*(1-fraction),
        alpha or a0*fraction + a1*(1-fraction))


def basic_color_sequence(x):
    """
         x < -1: black
    -1 < x <  0: shades of transparent black
         x == 0: white
    -1 < x <  0: shades of gray
         x == 1: black
     1 < x     : different colors and their shades cycling
    """
    if x in COLOR_LIBRARY:
        return Color.from_RGB(*COLOR_LIBRARY[int(x)])
    if x < -1:
        return Color.from_RGB(0, 0, 0, 255)
    elif x < 0:
        return Color.from_RGB(0, 0, 0, -x*255)
    elif x < 1:
        gray = 255 - x*255
        return Color.from_RGB(gray, gray, gray, 255)
    else:
        x = (x-1) % 9
        return merge(COLOR_LIBRARY[int(x)+2], COLOR_LIBRARY[int(x)+1], x % 1)


def hsv2rgb(h, s=255, v=255, alpha=255):
    c = v*s/255
    m = v-c
    hh = h*6
    x = c * (1 - abs(hh%2 - 1))
    if hh<1:   r, g, b = c, x, 0
    elif hh<2: r, g, b = x, c, 0
    elif hh<3: r, g, b = 0, c, x
    elif hh<4: r, g, b = 0, x, c
    elif hh<5: r, g, b = x, 0, c
    else:      r, g, b = c, 0, x
    return Color.from_RGB(r+m, g+m, b+m, alpha)


def random_color(alpha=255):
    import random
    return hsv2rgb(h=random.random(),
                   s=random.randint(0, 100) + 155,
                   v=random.randint(0, 100) + 155,
                   alpha=alpha)


def hash_color(x, alpha=255):
    h = (hash(x)+hash(str(x)))**2
    return hsv2rgb(h=((634*h) % 1000) / 1000,
                   s=(932*h) % 100 + 155,
                   v=(421*h) % 100 + 155,
                   alpha=alpha)


class ColorBrewer():
    # merges colors along a gradient
    def __init__(self, *gradient_points):
        self.gradient_points = gradient_points

    def __call__(self, x, alpha=None):
        # point lies outsise defined area
        if x < self.gradient_points[0][0]:
            return Color(*self.gradient_points[0][1])
        if x > self.gradient_points[-1][0]:
            return Color(*self.gradient_points[-1][1])

        for (x0, c0), (x1, c1) in zip(self.gradient_points[:-1], self.gradient_points[1:]):
            if x0 <= x <= x1:
                fraction = (x1-x)/(x1-x0)
                return merge(c0, c1, fraction, alpha=alpha)


FUNCTION_LIBRARY = {
    'hsv': hsv2rgb,
    'random': random_color,
    'hash': hash_color,
    'sequence': basic_color_sequence,

    'alpha': ColorBrewer(
        (0, 'a'),
        (1, 'k')),
    'gray': ColorBrewer(
        (0, 'w'),
        (1, 'k')),
    'heat': ColorBrewer(
        (0.00, 'pure blue'),
        (1/3,  'pure cyan'),
        (2/3,  'pure yellow'),
        (1.00, 'pure red')),
    'night': ColorBrewer(
        (0.00, (  0,   0, 50)),
        (0.25, (  0, 150, 150)),
        (0.50, (200, 200,   0)),
        (0.75, (255, 100, 100)),
        (1.00, (255, 255, 255))),
    'corr': ColorBrewer(
        (-1, 'pure blue'),
        ( 0, 'white'),
        ( 1, 'pure red')),
    'hipst': ColorBrewer(
        (0.0, (100, 100, 255)),
        (0.5, (150, 150, 150)),
        (1.0, (255, 170, 10))),
    'black body': ColorBrewer(
        (0.0,            (  0,   0,   0)),
        (0.142857142857, ( 43,  15, 107)),
        (0.285714285714, ( 93,   0, 200)),
        (0.428571428571, (198,   0, 116)),
        (0.571428571429, (235,  83,  60)),
        (0.714285714286, (245, 151,  48)),
        (0.857142857143, (233, 216,  57)),
        (1.0,            (255, 255, 255)))
    }


class Color(tuple):

    def __new__(cls, first_arg, *rest_args):
        if not rest_args:
            # this case covers sending Color as well
            if isinstance(first_arg, tuple):
                return cls(*first_arg)

            # check integers from Color Library
            if isinstance(first_arg, (int, float)):
                return basic_color_sequence(first_arg)

            # None
            elif first_arg is None:
                return None

        # just ignore None, if it is the first, but not the only argument
        if first_arg is None:
            return cls(*rest_args)

        # color is represented in RGB (or RGBA) form
        if isinstance(first_arg, (int, float)):
            return cls.from_RGB(first_arg, *rest_args)

        # color is either coming from Color library or from function library
        if isinstance(first_arg, str):
            return cls.from_function(first_arg, *rest_args)

        raise KeyError(f'wrong argument in Color(): {(first_arg, ) + rest_args}')

    @classmethod
    def from_RGB(cls, r, g, b, a=255):
        r = _to_0_255(r)
        g = _to_0_255(g)
        b = _to_0_255(b)
        a = _to_0_255(a)
        return tuple.__new__(cls, (r, g, b, a))

    @classmethod
    def from_function(cls, func, *args):
        if func in COLOR_LIBRARY:
            return cls.from_RGB(*COLOR_LIBRARY[func], *args)

        elif func in FUNCTION_LIBRARY:
            return FUNCTION_LIBRARY[func](*args)

        # modifications
        elif func.startswith('-') and args and isinstance(args[0], (int, float)):
            # special mod for color brewers
            return cls.from_function(func[1:], 1-args[0], *args[1:])

        elif func.startswith('^^^'): return cls.from_function(func[3:], *args).to_white(0.9)
        elif func.startswith('^^'):  return cls.from_function(func[2:], *args).to_white(0.5)
        elif func.startswith('^'):   return cls.from_function(func[1:], *args).to_white(0.25)

        elif func.startswith('...'): return cls.from_function(func[3:], *args).to_black(0.9)
        elif func.startswith('..'):  return cls.from_function(func[2:], *args).to_black(0.5)
        elif func.startswith('.'):   return cls.from_function(func[1:], *args).to_black(0.25)

        elif func.startswith('=='):  return cls.from_function(func[1:], *args).to_gray(0.9)
        elif func.startswith('='):   return cls.from_function(func[1:], *args).to_gray(0.5)

        elif func.startswith('___'): return cls.from_function(func[3:], *args).alpha(0.1)
        elif func.startswith('__'):  return cls.from_function(func[2:], *args).alpha(0.5)
        elif func.startswith('_'):   return cls.from_function(func[1:], *args).alpha(0.75)


    def to_white(self, x):
        return merge((255, 255, 255, 255), self, x)
    def to_black(self, x):
        return merge((0, 0, 0, 255), self, x)
    def to_gray(self, x):
        return merge((128, 128, 128, 255), self, x)

    def merge(self, other, w=0.5):
        return merge(self, other, w)
    def alpha(self, a):
        r, g, b, _ = self
        return Color(r, g, b, a*255)

    def power(self, x):
        if x < 1:
            return self.alpha(x)
        else:
            return self.to_black(min(x-1, 0.3))

    def text_color(self):
        if sum(self)<709:
            return Color.from_RGB(255, 255, 255)
        else:
            return Color.from_RGB(0, 0, 0)

    def opposite(self):
        r, g, b, a = self
        return Color.from_RGB(255-r, 255-g, 255-b, a)

    def __repr__(self):
        r, g, b, a = self
        return f'color({r}, {g}, {b}, {a})'


class ColorDescriptor:
    def __set_name__(self, owner, name): self.name = name
    def __get__(self, obj, cls): return obj.__dict__[self.name]
    def __set__(self, obj, value):
        obj.__dict__[self.name] = Color(value)
