import numpy
import numpy.random as rnd

import sys
import os

def median(L):
    L =sorted(L)
    return L[len(L)//2]

# -- sampling

def U01():
    return rnd.random()
def U02():
    return 2*rnd.random()
def U11():
    return 2*rnd.random()-1

def d6():
    return rnd.randint(6)+1
def d100():
    return rnd.randint(100)+1

from numpy.random import randint


def rmulnorm(x,cov):
    return numpy.random.multivariate_normal(x, cov).tolist()


def assets_path(filename):
    if getattr(sys, 'frozen', False):
        # The application is frozen
        datadir = os.path.dirname(sys.executable)
    else:
        # The application is not frozen
        # Change this bit to match where you store your data files:
        datadir = os.path.dirname(__file__)
    return os.path.join(datadir, 'assets', filename)