import pyglet
from pyglet.gl import *
from pyglet.window import key

import numpy
from numpy import cos, sin, exp, pi
from numpy import matrix, array, mean

import useful
import color as color_lib


from random import sample


class Window(pyglet.window.Window):

    # Cube 3D start rotation
    xRotation = 25
    yRotation = 0
    speed= 0
    stop=False
    press_up = press_down = press_right = press_left = False

    def __init__(self, width, height, title='Mouse Hunt'):
        super(Window, self).__init__(width, height, title)
        glClearColor(0, 0, 0, 1)
        glEnable(GL_DEPTH_TEST)

        icon = pyglet.image.load(useful.assets_path('icon.png'))
        self.set_icon(icon)

    def on_draw(self):
        # Clear the current GL Window
        self.clear()

        # Push Matrix onto stack
        glPushMatrix()

        glRotatef(self.xRotation, 1, 0, 0)
        glRotatef(self.yRotation, 0, 1, 0)

        show_landscape()

        # Pop Matrix off stack
        glPopMatrix()

    def on_resize(self, width, height):
        # set the Viewport
        glViewport(0, 0, width, height)

        # using Projection mode
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()

        aspectRatio = width / height
        gluPerspective(35, aspectRatio, 1, 1000)

        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        glTranslatef(0, 0, -150)

        glEnable( GL_MULTISAMPLE )
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glEnable(GL_TEXTURE_2D)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    def on_key_press(self,symbol, modifiers):
        if self.stop:
            return
        if symbol == key.SPACE:
            if not G.mouse:
                G.init()
                return
            self.press_left = False
            self.press_right = False
            self.speed=0
            self.stop=True
            G.jump()

        elif symbol == key.UP:		self.press_up = True
        elif symbol == key.DOWN:	self.press_down = True
        elif symbol == key.LEFT:	self.press_left = True
        elif symbol == key.RIGHT:	self.press_right = True
        elif symbol == key.ESCAPE:	quit()
        elif symbol == key.R:
            G.init()

    def on_key_release(self,symbol, modifiers):
        if symbol == key.UP:		self.press_up = False
        elif symbol == key.DOWN:	self.press_down = False
        elif symbol == key.LEFT:	self.press_left = False
        elif symbol == key.RIGHT:	self.press_right = False


def go_on():
    W.stop = False
    G.jumping = False
    G.raising = False

    pyglet.clock.unschedule(jump)


restart_count=0
def mouse(delta):
    if not G.mouse: return
    if G.jumping: return
    if useful.d6()<4: return
    r = useful.randint(len(sounds_l))
    global PLAYERS, restart_count
    restart_count+=1
    if restart_count>10:
        restart_count=0
        PLAYERS = [pyglet.media.Player(), pyglet.media.Player()]

    PLAYERS[0].queue(sounds_l[r])
    PLAYERS[1].queue(sounds_r[r])
    for P in PLAYERS:
        P.play()


def jump(delta):
    G.sprite +=  delta*10
    if G.raising:
        if G.sprite>25:
            go_on()
        if G.sprite>9 and G.mouse:
            go_on()
    else:
        if G.sprite>15 and G.snow:
            G.catch()
        elif G.sprite>9 and not G.snow:
            go_on()


def rotate(delta):
    if W.press_up: 	W.speed = max(1,W.speed+delta*5)
    if W.press_down: 	W.speed = min(-1,W.speed-delta*5)
    if not (W.press_up or W.press_down):
        if abs(W.speed)<2:
            W.speed = 0
        else:
            W.speed*=0.9
    W.yRotation+=W.speed

    if W.press_left or W.press_right:
        x,y,z = G.get_fox()
        G.speed = min(3,G.speed+delta*2)
        #G.speed =0
        if W.press_left:
            G.direction=1
            x -= delta * cos(W.yRotation/180*pi) * G.speed
            y -= delta * sin(W.yRotation/180*pi) * G.speed
        if W.press_right:
            G.direction=0
            x += delta * cos(W.yRotation/180*pi) * G.speed
            y += delta * sin(W.yRotation/180*pi) * G.speed
        G.fox=x
        G.foy=y
        G.foz=G.get_z()
        G.move  = True
        G.sprite = (G.sprite + delta*7)%6
    else:
        G.move  = False
        G.speed = 1

    sx,sy,sz =  G.sound_pos[0]-G.fox, G.sound_pos[1]-G.foy, G.sound_pos[2]-G.foz
    sx,sy = to_reality(sx,sy)
    sx,sy = cos(W.yRotation/180*pi)*sx+ sin(W.yRotation/180*pi)*sy, cos(W.yRotation/180*pi)*sy - sin(W.yRotation/180*pi)*sx

    dist = (sx**2+sy**2)**0.5

    master = exp(- dist)
    rot = sx/dist

    PLAYERS[1].volume = (rot/2+0.5)*master
    PLAYERS[0].volume = (-rot/2+0.5)*master



pyglet.clock.schedule(rotate)
pyglet.clock.schedule_interval(mouse,1)


########## create landscape #############

step = 0.75**0.5
def to_reality(x,y):
    return x*step,y

def distance(i,j,x,y):
    i,j = to_reality(i,j)
    x,y = to_reality(x,y)
    return ((i-x)**2 + (j-y)**2)**0.5

def make_grid(field_size=6):
    global coords
    global sigma
    global triangles

    coords = [ [i,j+(i%2)/2] for i in range(-10,11) for j in range(-10,11)]
    coords = [ [x,y] for x,y in coords if max([abs(x),abs(y-x/2),abs(y+x/2)])<=field_size]
    correlation = 10**-useful.d6()

    sigma = matrix([[exp(-distance(i,j,x,y)/5) + (correlation if (i,j)==(x,y) else 0) for i,j in coords] for x,y in coords])

    triangles = []
    for x,y in coords:
        if [x+1,y+0.5] in coords and [x+1,y-0.5] in coords:
            triangles.append([[x,y,0],[x+1,y+0.5,0],[x+1,y-0.5,0],0])
        if [x-1,y+0.5] in coords and [x-1,y-0.5] in coords:
            triangles.append([[x,y,0],[x-1,y-0.5,0],[x-1,y+0.5,0],0])


def color(abc, base_col, shade, snow_col):
    a, b, c = abc
    p1 = array(b)-array(a)
    p2 = array(c)-array(a)
    n = numpy.cross(p1,p2)
    n = n/numpy.linalg.norm(n)
    h = -numpy.dot(n, array([0.707, 0, 0.707]))

    mid = mean([a[2],b[2]])
    if a[2]<0 and b[2]<0 and c[2]<0:
        snow = True
        rgb =  snow_col.merge(shade, h/2+0.5)[:3]
    else:
        snow = False
        rgb =  base_col.merge(shade, h)[:3]

    rgb = [min(255,x) for x in rgb]
    rgb = [max(0,  x) for x in rgb]
    rgb = [int(x) for x in rgb]
    return rgb, snow


class Game():
    direction=0
    sprite = 0
    speed=1
    move=False
    jumping=False
    raising=False

    def __init__(self):
        self.init(first=True)

    def init(self, first=False):
        filed_size = 5 if first else useful.randint(5, 9)
        make_grid(filed_size)

        land = useful.rmulnorm([0 for i in coords], sigma)
        land = [h-useful.median(land) for h in land]
        land = [h/3 if h<0 else h for h in land]
        self.land=land

        global h_scale
        h_scale = (20 + 10*useful.U01()) / (max(land) - min(land))
        if first:
            h_scale = 15  / (max(land) - min(land))

        #randomize color
        hue = useful.U01()/2 - 0.3
        base_col = color_lib.hsv2rgb(hue, useful.U01()*200 + 55, useful.U01()*200 + 55)
        shade_col = color_lib.hsv2rgb(useful.U01(), 100, 100)
        if first:
            base_col = color_lib.Color(180, 122, 90)
            shade_col = color_lib.Color(60, 60, 100)
        snow_col = color_lib.Color('w').merge(color_lib.hsv2rgb(hue, 255, 255), 0.85)
        #global triangles
        rgb = [useful.U01()*50 for i in range(3)]
        possible_pos = []
        for h in triangles:
            for xyz in h[:3]:
                xyz[2] = land[coords.index(xyz[:2])]
            h[3], snow=color(h[:3], base_col, shade_col, snow_col)
            if snow: possible_pos.append(h)

        if len(possible_pos)==0:
            self.init()
        else:
            mouse_pos = sample(possible_pos,1)[0]
            self.mouse_tri = mouse_pos
            self.sound_pos = [mean([mouse_pos[i][j] for i in range(3)]) for j in range(3)]

            self.fox = 0
            self.foy = 0
            self.foz = self.get_z()

            self.mouse=True

    def get_z(self):
        x = int(self.fox+100)-100
        y = int(self.foy+100)-100
        dx = self.fox%1
        dy = self.foy%1 *2
       # print x,y
        if [x,y] in coords:
            if   dy<dx:   tri = [[x+1,y+0.5],[x,y],[x+1,y-0.5]]
            elif dy>2-dx: tri = [[x+1,y+0.5],[x,y+1],[x+1,y+1.5]]
            else:         tri = [[x+1,y+0.5],[x,y],[x,y+1]]
        else:
            if   dy<1-dx: tri = [[x,y+0.5],[x+1,y],[x,y-0.5]]
            elif dy>1+dx: tri = [[x,y+0.5],[x+1,y+1],[x,y+1.5]]
            else:         tri = [[x,y+0.5],[x+1,y+1],[x+1,y]]

        land = self.land
        try:
            hhh = [land[coords.index(xy)] for xy in tri]
        except:
            return 0
        a,b,c = tri[0]+[hhh[0]]
        p1 = array(tri[1]+[hhh[1]])-array([a,b,c])
        p2 = array(tri[2]+[hhh[2]])-array([a,b,c])
        n = numpy.cross(p1,p2)

        self.snow = all([h<0 for h in hhh])

        z = c - (n[0]*(self.fox-a) + n[1]*(self.foy-b))/n[2]
        return z

    def get_fox(self):
        return self.fox, self.foy, self.foz

    def get_sprite(self):
        if self.raising: 	return 0+ min(19,int(self.sprite))# - 10*self.snow
        if self.jumping: 	return 30+ min(9,int(self.sprite)) - 10*(self.snow and self.mouse)
        if self.move: 		return 41+ int(self.sprite) + 10*(self.snow and self.mouse)
        return 40 + 10*(self.snow and self.mouse)

    def jump(self):
        self.speed=1
        self.sprite=0
        self.jumping=True
        pyglet.clock.schedule(jump)

    def catch(self):
        sx,sy,sz =  G.sound_pos[0]-G.fox, G.sound_pos[1]-G.foy, G.sound_pos[2]-G.foz
        sx,sy = to_reality(sx,sy)
        dist = (sx**2+sy**2)**0.5
        self.raising = True
        G.sprite = 0
        if dist<1:
            self.mouse=False
            print('success!!')
        else:
            print('fail')
        #go_on()



def show_landscape():
    def trans(x,y,z):
        x,y = to_reality(x,y)
        return x*10, z*h_scale, y*10
    glBegin(GL_TRIANGLES)
    i=0
    for v1,v2,v3,c in triangles:
        i+=5
        glColor3ub(*c)
        for x,y,z in v1,v2,v3:
            glVertex3f(*trans(x,y,z))

    glEnd()

    glColor3ub(255,255,225)
    glEnable( GL_TEXTURE_2D );
    texture = fox_textures[G.get_sprite()]
    glBindTexture(texture.target, texture.id)
    glBegin( GL_QUADS );

    x,y,z = G.get_fox()
    x,z,y = trans(x,y,z)
    d = G.direction
    foxsize=3
    dx,dy = cos(W.yRotation/180*pi)*foxsize, sin(W.yRotation/180*pi)*foxsize
    glTexCoord2d(0.01+d*0.98,0.99); glVertex3d(x-dx,z+foxsize*2.2,y-dy);
    glTexCoord2d(0.99-d*0.98,0.99); glVertex3d(x+dx,z+foxsize*2.2,y+dy);
    glTexCoord2d(0.99-d*0.98,0.01); glVertex3d(x+dx,z,y+dy);
    glTexCoord2d(0.01+d*0.98,0.01); glVertex3d(x-dx,z,y-dy);

    glEnd();
    glDisable( GL_TEXTURE_2D );


fox_image = pyglet.image.load(useful.assets_path('sprite.png'))
fox_seq = pyglet.image.ImageGrid(fox_image, 6, 10)
fox_textures = [image.get_texture() for image in fox_seq]

#pyglet.options['audio'] = ('directsound','openal', 'silent')
sounds_l = [pyglet.media.load(useful.assets_path('sound_'+s+'l.wav'), streaming=False) for s in '1234568' ]
sounds_r = [pyglet.media.load(useful.assets_path('sound_'+s+'r.wav'), streaming=False) for s in '1234568' ]
PLAYERS = [pyglet.media.Player(), pyglet.media.Player()]



G = Game()
W = Window(1340, 600, 'Mouse Hunt')
pyglet.app.run()

